var express = require("express");
var app = express();

app.use(express.static("public"));
app.set("view engine", "ejs");

app.get("/", function(req, res){
  res.render("home");
});

app.get("/atoms", function(req, res){
  res.render("atoms");
});
//
// app.get("/contact", function(req, res){
//   res.render("contact");
// });
//
app.get("*", function(req,res) {
  res.send("You are a star!")
});

app.listen(3000, function(){
  console.log("Server has started!!!")
});
